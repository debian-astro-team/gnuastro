Source: gnuastro
Section: science
Priority: optional
Maintainer: Debian Astro Team <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Mohammad Akhlaghi <mohammad@akhlaghi.org>,
                    Phil Wyett <philip.wyett@kathenas.org>
Build-Depends: debhelper-compat (= 13),
               ghostscript,
               libcfitsio-dev,
               libgit2-dev,
               libgsl0-dev,
               libjpeg-dev,
               libtiff-dev,
               libtool,
               wcslib-dev
Rules-Requires-Root: no
Standards-Version: 4.7.0
Homepage: https://www.gnu.org/software/gnuastro
Vcs-Git: https://salsa.debian.org/debian-astro-team/gnuastro.git
Vcs-Browser: https://salsa.debian.org/debian-astro-team/gnuastro

Package: libgnuastro21
Replaces: libgnuastro20t64, libgnuastro20, libgnuastro18, libgnuastro17
Breaks: libgnuastro20t64 (<< ${source:Version}), libgnuastro20 (<< ${source:Version}),
        libgnuastro18 (<< ${source:Version}), libgnuastro17 (<< ${source:Version})
Architecture: any
Section: libs
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: GNU Astronomy Utilities shared libraries
 GNU Astronomy Utilities (Gnuastro) is a collection of libraries and programs
 for astronomical data analysis and manipulation. The programs can be run on
 the command-line for efficient and easy usage and the libraries can be used
 within C and C++ programs.
 .
 This package contains Gnuastro's shared librarires.

Package: libgnuastro-dev
Replaces: libgnuastro20t64, libgnuastro20, libgnuastro18, libgnuastro17
Breaks: libgnuastro20t64 (<< ${source:Version}), libgnuastro20 (<< ${source:Version}),
        libgnuastro18 (<< ${source:Version}), libgnuastro17 (<< ${source:Version})
Architecture: any
Section: libdevel
Depends: libgnuastro21 (= ${binary:Version}), ${misc:Depends}
Description: GNU Astronomy Utilities development files
 GNU Astronomy Utilities (Gnuastro) is a collection of libraries and programs
 for astronomical data analysis and manipulation. The programs can be run on
 the command-line for efficient and easy usage and the libraries can be used
 within C and C++ programs.
 .
 This package contains Gnuastro's headers and static libraries.

Package: gnuastro
Architecture: any
Depends: libgnuastro21 (= ${binary:Version}), ${misc:Depends}, ${shlibs:Depends}
Description: GNU Astronomy Utilities programs
 GNU Astronomy Utilities (Gnuastro) is a collection of libraries and programs
 for astronomical data analysis and manipulation. The programs can be run on
 the command-line for efficient and easy usage and the libraries can be used
 within C and C++ programs.
 .
 This package contains Gnuastro's programs.
